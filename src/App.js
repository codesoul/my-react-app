import './App.css';
import { createMuiTheme, CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import RouterComponent from './routes/Router';


const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ff4400',
      light: '#D4F1F4'
    },
    secondary: {
      main: '#ff4400',
      light: '#f8324526'
    },
    background: {
      default: '#f4f5fd'
    }

  }
})


function App() {
  return (
    <ThemeProvider theme={theme}>
      {/* <WebsiteMain /> */}
      <RouterComponent />

      <CssBaseline />
    </ThemeProvider>
  );
}

export default App;
