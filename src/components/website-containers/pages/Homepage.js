import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';

import SecondaryNav from './homepage-contents/SecondaryNav';
import CategoryHeader from './homepage-contents/CategoryHeader';
import CategoryBody from './homepage-contents/CategoryBody';
import Footer from './homepage-contents/Footer';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: theme.palette.primary.light,
        }
    })
)

export default function Homepage() {
    const classes = useStyles();
    return (
        <div>
            <Box width="100%" className={classes.root} overflow="hidden">
                <SecondaryNav />
                <CategoryHeader />
                <Grid container spacing={3}>
                    {[...Array(10)].map((x, i) =>
                        <Grid item xs={6} sm={2} spacing={3} key={i}>
                            <CategoryBody />
                        </Grid>
                    )
                    }
                </Grid>
                <Footer />
            </Box>
        </div>
    )
}
