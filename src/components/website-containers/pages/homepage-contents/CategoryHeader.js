import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    center:{
         textAlign:'center'   
    }
}));

export default function CategoryHeader() {
    const classes = useStyles();

    return (
        <div className = {classes.center}>
            <h1>Grocery</h1>
        </div>
    )
}
