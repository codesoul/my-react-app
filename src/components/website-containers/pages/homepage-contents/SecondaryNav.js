import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    center:{
         textAlign:'center'   
    }
}));

export default function SecondaryNav() {
    const classes = useStyles();
    return (
        <div className = {classes.center}>
            <Grid container spacing={3}>
                <Grid item xs={6} sm={2} spacing={3} >
                    <Link to="/home"><Button color="inherit">Grocery</Button></Link>
                </Grid>
                <Grid item xs={6} sm={2} spacing={3} >
                    <Link to="/home"><Button color="inherit">Electronics</Button></Link>
                </Grid>
                <Grid item xs={6} sm={2} spacing={3} >
                    <Link to="/home"><Button color="inherit">Fashions</Button></Link>
                </Grid>
                <Grid item xs={6} sm={2} spacing={3} >
                    <Link to="/home"><Button color="inherit">Home decor</Button></Link>
                </Grid>
                <Grid item xs={6} sm={2} spacing={3} >
                    <Link to="/home"><Button color="inherit">Stationary</Button></Link>
                </Grid>
            </Grid>
        </div>
    )
}
