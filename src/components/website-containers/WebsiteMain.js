import React from 'react';
import Header from './components/Header';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: theme.palette.primary.light,
        }
    })

)

const WebsiteMain = props =>{
    const classes = useStyles();
    return(
        <div className = "fullHeight">
            <Header {...props} />
            <Box  width="100%" className = {classes.root} overflow="hidden">
                {props.children}
            </Box>    
            {/* <Footer /> */}
        </div>
    );
};

export default WebsiteMain
