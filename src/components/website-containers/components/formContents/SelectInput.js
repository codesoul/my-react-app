import React, { useState } from 'react';
import { FormControl, InputLabel, Select, MenuItem, FormHelperText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  formControl: {
    width: '100%'
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function SelectInput(props) {
  const classes = useStyles();

  const {name, label, value,  onChange, options} = props

 

  return (
    <div>
      <FormControl 
          variant="outlined" 
          className={classes.formControl}
         

          >
        <InputLabel >{label}</InputLabel>
        <Select
          label={label}
          name ={name}
          value = {value}
          onChange = {onChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {
            options.map(
              item =>(
                <MenuItem key = {item.id} value = {item.id}>{item.title}</MenuItem>
              )
            )
          }
        </Select>
      </FormControl>
    </div>
  )
}
