import React, {useState} from 'react'

export function FormOperations(initialValues, validateOnChange = false, validate) {
    
    const [formData, setFormData] = useState(initialValues);
    const [errors, setErrors] = useState({});
    var arr = formData["skills"]

    const handleInputChange = e =>{
        console.log(arr)
        console.log(e)
        const {name, value} = e.target
        setFormData({
            ...formData,
            [name]:value
        })
        if(validateOnChange)
            validate({[name]: value})
    }

    const handleClickShowPassword = () => {
        setFormData({ ...formData, showPassword: !formData.showPassword });
      };
    
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };

    const addSkills = () =>{
        
    }  
    
    return {
        formData,
        setFormData,
        errors,
        setErrors,
        handleInputChange,
        handleClickShowPassword,
        handleMouseDownPassword
    }
}



export function Form (props){

    const {children , ...other} = props;

    return (
        <form  autoComplete = "off" {...other}> 
            {props.children}
        </form>
    )

}
