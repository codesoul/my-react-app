import React from 'react'
import { makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
// import LoginCard from '../formsUsed/LoginCard';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  imgContainer:{
    width:100
  },
  bgImage:{
    // backgroundImage:`url("https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg")`,
    height:600,
    backgroundPosition:'center',
    backgroundRepeat:'no-repeat',
    backgroundSize:'cover',
    paddingTop:'3%'
  },
  card:{
    width:'100%',
    backgroundColor:theme.primary
  }
}));


export default function ImageGalary() {

  const classes = useStyles();
  return (
    <div className={classes.root} >
     
            
              <Grid item xs = {9} sm = {4} style = {{margin:'auto'}}>
                {/* <Card className={classes.card}></Card> */}
                {/* <LoginCard/> */}
              </Grid>
          
       
        {/* <Grid item xs={6} sm = {4}>
          <Paper className={classes.paper}>xs=6</Paper>
        </Grid>
        <Grid item xs={6} sm = {4}>
          <Paper className={classes.paper}>xs=6</Paper>
        </Grid>
        <Grid item xs={3} sm = {4}>
          <Paper className={classes.paper}>xs=3</Paper>
        </Grid>
        <Grid item xs={3} sm = {4}>
          <Paper className={classes.paper}>xs=3</Paper>
        </Grid>
        <Grid item xs={3} sm = {4}>
          <Paper className={classes.paper}>xs=3</Paper>
        </Grid>
        <Grid item xs={3} sm = {4}>
          <Paper className={classes.paper}>xs=3</Paper>
        </Grid> */}
 
    </div>
  )
}
