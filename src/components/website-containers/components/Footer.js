import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { sizing } from '@material-ui/system';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: theme.palette.primary.main,
        }
    })

)

export default function Footer() {

    const classes = useStyles();
    return (
        <div>
            <Box height={100} width="100%" className = {classes.root}>
                
            </Box>
        </div>
    )
}
