import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import Input from '../formContents/Input';
import SelectInput from '../formContents/SelectInput';
import AccountCircle from '@material-ui/icons/AccountCircle';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import Button from '../formContents/Button';
import { FormOperations, Form } from '../formContents/formOperations';
import RadioInput from '../formContents/RadioInput';

const designation = [
  {id: 'software_developer' , title : 'Software Developer'},
  {id: 'human_resources' , title : 'Human Resources'}
]


const genderGroups = [
  {id: 'male', title:'Male'},
  {id: 'female', title:'Female'},
  {id:'others', title:'Others'}
]

const countryList = [
  {id:'nepal', title:'Nepal'},
  {id:'india', title:'India'}
]

const StateList =[
  {id: 'nawalparasi', title:'Nawalparasi'},
  {id:'rupendehi', title:'Rupendehi'}
]


const useStyles = makeStyles((theme) => ({
  card: {
    width: "60%",
    margin: 'auto',
    padding: '4%',
    marginTop: '6%',
    marginBottom: '5%'
  },
  margin: {
    marginBottom: theme.spacing(1)
  },
  inputWidth: {
    width: '100%',
    marginBottom: theme.spacing(1)
  }
}));

const initialValues = {
  fname: '',
  lname: '',
  email: '',
  phoneNumber: '',
  designation: '',
  gender: 'male',
  country: '',
  state: '',
  skills: ['dfsf',],
  job_type: '',
  password: '',
  confirmPassword: '',
  showPassword: true
}

export default function SignUp() {

  const validate = (fieldValues = formData) => {
    let temp = { ...errors }
    if ('email' in fieldValues)
      temp.email = (/$^|.+@.+..+/).test(fieldValues.email) && fieldValues.email ? "" : "Email is not valid."

    if ('password' in fieldValues)
      temp.password = fieldValues.password ? "" : "This field is required"

    setErrors({
      ...temp
    })

    if (fieldValues == formData)
      return Object.values(temp).every(x => x == "")
  }


  const classes = useStyles();



  const { formData, setFormData, handleInputChange, handleClickShowPassword, handleMouseDownPassword, errors, setErrors } = FormOperations(initialValues, true, validate)





  const handleSubmit = e => {
    e.preventDefault();
    if (validate()) {
      console.log(formData)
    }

  }

  return (
    <div>

      <Card className={classes.card}>
        <h1>Sign Up Form</h1>

        <Form onSubmit={handleSubmit}>
          <Grid container direction="row" spacing={3}>

            <Grid item xs={6}>
              <Input
                type = "text"
                name = "fname"
                label="First Name"
                value ={formData.fname}
                onChange={handleInputChange}
                className={classes.inputWidth} />
            </Grid>
            
            
            <Grid item xs={6}>
              <Input
                type = "text"
                name = "lname"
                label="Last Name"
                value = {formData.lname}
                onChange={handleInputChange}
                className={classes.inputWidth} />
            </Grid>

            <Grid item xs={6}>
              <Input
                type="text"
                name='email'
                label="Email"
                value={formData.email}
                onChange={handleInputChange}
                error={errors.email}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <AccountCircle />
                    </InputAdornment>
                  ),
                }}
                className={classes.inputWidth} />
            </Grid>

            <Grid item xs={6}>
              <Input
                type = "number"
                name = "phoneNumber"
                label="Phone Number"
                value={formData.phoneNumber}
                onChange={handleInputChange}
                className={classes.inputWidth}
              />
            </Grid>

            <Grid item xs={6}>
                <SelectInput 
                name = "designation"
                label = "Designation"
                value = {formData.designation}
                onChange = {handleInputChange}
                options = {genderGroups}
                className={classes.inputWidth}
                />
            </Grid>

            <Grid item xs={6}>
              <RadioInput 
               name="gender"
               label = "Gender"
               value={formData.gender}
               onChange={handleInputChange}
               items = {genderGroups} />
            </Grid>

            <Grid item xs={6}>
            <SelectInput 
                name = "country"
                label = "Country"
                value = {formData.country}
                onChange = {handleInputChange}
                options = {countryList}
                className={classes.inputWidth}
                />
            </Grid>

            <Grid item xs={6}>
            <SelectInput 
                 name = "state"
                 label = "State"
                 value = {formData.state}
                 onChange = {handleInputChange}
                 options = {StateList}
                 className={classes.inputWidth}
                />
            </Grid>

            <Grid item xs={6}>
              <Input
                name = "skills"
                label="Skills"
                value = {formData.skills}
                onChange={handleInputChange}
                className={classes.inputWidth} 
                 InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                       <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        <AddIcon />
                      </IconButton>

                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                         <RemoveIcon />
                      </IconButton>


                     
                    </InputAdornment>
                    
                  ),
                }}
                />
            </Grid>

            <Grid item xs={6}>
              <Input
                type={formData.showPassword ? 'text' : 'password'}
                name='password'
                label="Password"
                value={formData.password}
                onChange={handleInputChange}
                error={errors.password}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {formData.showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
                className={classes.inputWidth}
              />
            </Grid>

            <Grid item xs={6}>
              <Input
                label="Confirm Password"
                className={classes.inputWidth} />
            </Grid>

            <Grid item xs={6}>
              
            </Grid>



            <Button
              text="Submit"
              type="submit"
              className={classes.margin}
            />

            <a className={classes.marginAuto}>Register</a>

          </Grid>
        </Form>


      </Card>

    </div>
  )
}
