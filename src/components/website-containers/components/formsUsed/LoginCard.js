import React, {useState} from 'react';
import { makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import Input from '../formContents/Input';
import AccountCircle from '@material-ui/icons/AccountCircle';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import Button from '../formContents/Button';
import {FormOperations,Form } from '../formContents/formOperations'


const useStyles = makeStyles((theme) => ({
    card:{
    width: "39%",
    margin: 'auto',
    padding: '4%',
    marginTop: '6%',
    marginBottom: '5%'
    },
    margin: {
      marginBottom: theme.spacing(1)
    },
    inputWidth:{
      width:'100%',
      marginBottom: theme.spacing(1)
    }
  }));
  
const initialValues = {
  email:'',
  password:'',
  showPassword:true
}

export default function LoginCard() {

    const validate = (fieldValues = formData) =>{
        let temp = {...errors}
        if('email' in fieldValues)
           temp.email = (/$^|.+@.+..+/).test(fieldValues.email) && fieldValues.email ?"":"Email is not valid."

        if('password' in fieldValues)
           temp.password = fieldValues.password?"":"This field is required" 

        setErrors({
         ...temp
        }) 
        
        if(fieldValues == formData)
            return Object.values(temp).every(x => x == "")
    }  


    const classes = useStyles();

  

    const {formData,setFormData, handleInputChange, handleClickShowPassword,handleMouseDownPassword, errors, setErrors } = FormOperations(initialValues, true, validate) 

    

  

  const handleSubmit = e =>{
    e.preventDefault();
    if(validate()){
      console.log(formData)
    }
    
  }

    return (
        <div>
             <Card className={classes.card}>
               <h1>Login Form</h1>
               <Form onSubmit = {handleSubmit}>
                
                                <Input  
                                type = "text"
                                name= 'email'
                                label = "Email"
                                value = {formData.email}
                                onChange = {handleInputChange}
                                error = {errors.email}
                                InputProps={{
                                  endAdornment: (
                                    <InputAdornment position="end">
                                      <AccountCircle />
                                    </InputAdornment>
                                  ),
                                }}
                                className = {classes.inputWidth} />
                          

                         
                                <Input  
                               type={formData.showPassword ?'text':'password'}
                                name= 'password'
                                label = "Password"
                                value = {formData.password}
                                onChange = {handleInputChange}
                                error = {errors.password}
                                InputProps = {{
                                  endAdornment: (
                                    <InputAdornment position="end">
                                      <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                      >
                                        {formData.showPassword ? <Visibility /> : <VisibilityOff />}
                                      </IconButton>
                                    </InputAdornment>
                                  )
                                }}
                                className = {classes.inputWidth}
                                />
                    

                        
                               <Button 
                               text = "Submit"
                               type = "submit"
                               className = {classes.margin}
                               />
                         
                    <a className={classes.marginAuto}>Register</a>
             
                
                </Form>
                
             </Card>
        </div>
    )
}
