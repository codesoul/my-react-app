import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import WebsiteMain from '../components/website-containers/WebsiteMain'
import LoginCard from '../components/website-containers/components/formsUsed/LoginCard';
import SignUp from '../components/website-containers/components/formsUsed/SignUp'
import Homepage from '../components/website-containers/pages/Homepage';


export default function RouterComponent() {
  return (
    <div>
      <BrowserRouter>
        <Switch>

          <Route path = "/home"  render={ () => 
            <WebsiteMain>
                <Homepage/>
            </WebsiteMain>
            }/>

            <Route path = "/login"  render={ () => 
            <WebsiteMain>
                <LoginCard/>
            </WebsiteMain>
            }/>

           <Route path = "/sign-up"  render={ () => 
            <WebsiteMain>
                <SignUp/>
            </WebsiteMain>
            }/>

        </Switch>
      </BrowserRouter>
    </div>
  )
}
